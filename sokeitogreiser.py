import sqlite3
import datetime
from datetime import datetime as dt

con = sqlite3.connect("jernbanesystemdb.db")
cursor = con.cursor()  

# Bruker skal kunne søke etter togruter som går mellom en startstasjon og en sluttstasjon, 
# med utgangspunkt i en dato og et klokkeslett. 
# Alle ruter den samme dagen og den neste skal returneres, sortert på tid. 

def sokeitogreiser():
    startstasjon = input("Hvilken jernbanestasjon skal reisen starte fra?: ")
    while (sjekkeGyldigeJernbanestasjoner(startstasjon) == False):
        startstasjon = input("Oppgitt jernbanestasjon er ugyldig. Prøv på nytt: ")
    
    endestasjon = input("Hvilken jernbanestasjon skal reisen slutte på?: ")
    while (sjekkeGyldigeJernbanestasjoner(endestasjon) == False):
        endestasjon = input("Oppgitt jernbanestasjon er ugyldig. Prøv på nytt: ")
    
    # Validering av dato 
    datoString = input("Hvilken dato er reisen? Format YYYY-MM-DD: ")
    while convertDato(datoString) == None:
        print("Ugyldig dato, skal være på formatet YYYY-MM-DD.")
        datoString = input("Hvilken dato er reisen? Format YYYY-MM-DD: ")

    dato = convertDato(datoString)
    datoIMorgen = dato + datetime.timedelta(days=1)
    
    # Validering av tid
    klokkeslett = input("Hvilket klokkeslett vil du søke fra? Format HH:MM: ")
    while (convertKlokkeslett(klokkeslett) == None):
        print("Ugyldig klokkeslett, skal være på formatet HH:MM.")
        klokkeslett = input("Hvilket klokkeslett vil du søke fra? Format HH:MM: ")
    
    # Sjekker om oppgitt startstasjon er av typen startstasjon, endestasjon eller mellomstasjon
    cursor.execute("SELECT JernbanestasjonNavn FROM Startstasjon WHERE JernbanestasjonNavn = ?", (startstasjon,))
    startstasjon1 = [item[0] for item in cursor.fetchall()]
    cursor.execute("SELECT JernbanestasjonNavn FROM Endestasjon WHERE JernbanestasjonNavn = ?", (startstasjon,))
    endestasjon1 = [item[0] for item in cursor.fetchall()]
    cursor.execute("SELECT JernbanestasjonNavn FROM Mellomstasjon WHERE JernbanestasjonNavn = ?", (startstasjon,))
    mellomstasjon1 = [item[0] for item in cursor.fetchall()]

    while (len(endestasjon1) > 0 and len(startstasjon1) == 0):
        print("Du har oppgitt en endestasjon som startstasjon, det starter ingen togruter her.")
        endestasjon = input("Prøv igjen, hvilken jernbanestasjon skal reisen starte fra?: ")
    
    # Sjekker om oppgitt endestasjon er av typen endestasjon eller mellomstasjon
    cursor.execute("SELECT JernbanestasjonNavn FROM Endestasjon WHERE JernbanestasjonNavn = ?", (endestasjon,))
    endestasjon2 = [item[0] for item in cursor.fetchall()]
    cursor.execute("SELECT JernbanestasjonNavn FROM Mellomstasjon WHERE JernbanestasjonNavn = ?", (endestasjon,))
    mellomstasjon2 = [item[0] for item in cursor.fetchall()]
    
    # Hvis startstasjonen er av type startstasjon og endestasjonen er av type mellomstasjon
    if (len(startstasjon1) > 0 and len(mellomstasjon2) > 0):
        avgangerIDag = finneTogruterFraStartstasjonTilMellomstasjon(startstasjon, endestasjon, dato, klokkeslett)
        avgangerIMorgen = finneTogruterFraStartstasjonTilMellomstasjonNesteDag(startstasjon, endestasjon, datoIMorgen)

    # Hvis startstasjonen er av type startstasjon og endestasjonen er av type endestasjon
    elif (len(startstasjon1) > 0 and len(endestasjon2) > 0):
        avgangerIDag = finneTogruterFraStartstasjonTilEndestasjon(startstasjon, endestasjon, dato, klokkeslett)
        avgangerIMorgen = finneTogruterFraStartstasjonTilEndestasjonNesteDag(startstasjon, endestasjon, datoIMorgen)
    
    # Hvis startstasjonen er av type mellomstasjon og endestasjonen er av type mellomstasjon
    elif (len(mellomstasjon1) > 0 and len(mellomstasjon2) > 0):
        avgangerIDag = finneTogruterFraMellomstasjonTilMellomstasjon(startstasjon, endestasjon, dato, klokkeslett)
        avgangerIMorgen = finneTogruterFraMellomstasjonTilMellomstasjonNesteDag(startstasjon, endestasjon, datoIMorgen)
    
    # Hvis startstasjonen er av type mellomstasjon og endestasjonen er av type endestasjon
    elif (len(mellomstasjon1) > 0 and len(endestasjon2) > 0):
        avgangerIDag = finneTogruterFraMellomstasjonTilEndestasjon(startstasjon, endestasjon, dato, klokkeslett)
        avgangerIMorgen = finneTogruterFraMellomstasjonTilEndestasjonNesteDag(startstasjon, endestasjon, datoIMorgen)
    
    avganger = avgangerIDag + avgangerIMorgen
    print("Her er togrutene som går fra", startstasjon, "til", endestasjon, "den", dato, "og den", datoIMorgen)
    print("[TogruteID, Startstasjon, Avgangstid, Endestasjon, Ankomsttid, Dato]")
    for entry in avganger:
        print(entry)


# HJELPEFUNKSJONER
def finneTogruterFraStartstasjonTilMellomstasjon(startstasjon, endestasjon, dato, klokkeslett):
    cursor.execute("""SELECT Togforekomst.TogruteID, Startstasjon.JernbanestasjonNavn, Startstasjon.avgangstid, Mellomstasjon.JernbanestasjonNavn, Mellomstasjon.ankomsttid, Togforekomst.Dato
                    FROM (((Mellomstasjon left outer join Startstasjon on Startstasjon.TogruteID == Mellomstasjon.TogruteID) inner join Togrute on Togrute.TogruteID == Startstasjon.TogruteID) inner join Togforekomst on Togforekomst.TogruteID == Togrute.TogruteID)
                    WHERE Startstasjon.JernbanestasjonNavn = ? and Mellomstasjon.JernbanestasjonNavn = ? and Togforekomst.Dato = ? and Startstasjon.Avgangstid >= ?
                    ORDER BY Startstasjon.Avgangstid ASC""", (startstasjon, endestasjon, dato, klokkeslett))
    return cursor.fetchall()

def finneTogruterFraStartstasjonTilMellomstasjonNesteDag(startstasjon, endestasjon, dato):
    cursor.execute("""SELECT Togforekomst.TogruteID, Startstasjon.JernbanestasjonNavn, Startstasjon.avgangstid, Mellomstasjon.JernbanestasjonNavn, Mellomstasjon.ankomsttid, Togforekomst.Dato
                    FROM (((Mellomstasjon left outer join Startstasjon on Startstasjon.TogruteID == Mellomstasjon.TogruteID) inner join Togrute on Togrute.TogruteID == Startstasjon.TogruteID) inner join Togforekomst on Togforekomst.TogruteID == Togrute.TogruteID)
                    WHERE Startstasjon.JernbanestasjonNavn = ? and Mellomstasjon.JernbanestasjonNavn = ? and Togforekomst.Dato = ?
                    ORDER BY Startstasjon.Avgangstid ASC""", (startstasjon, endestasjon, dato))
    return cursor.fetchall()

def finneTogruterFraStartstasjonTilEndestasjon(startstasjon, endestasjon, dato, klokkeslett):
    cursor.execute("""SELECT Togforekomst.TogruteID, Startstasjon.JernbanestasjonNavn, Startstasjon.avgangstid, Endestasjon.JernbanestasjonNavn, Endestasjon.ankomsttid, Togforekomst.Dato
                        FROM (((Endestasjon left outer join Startstasjon on Startstasjon.TogruteID == Endestasjon.TogruteID) inner join Togrute on Togrute.TogruteID == Startstasjon.TogruteID) inner join Togforekomst on Togforekomst.TogruteID == Togrute.TogruteID)
                        WHERE Startstasjon.JernbanestasjonNavn = ? and Endestasjon.JernbanestasjonNavn = ? and Togforekomst.Dato = ? and Startstasjon.Avgangstid >= ?
                        ORDER BY Startstasjon.Avgangstid ASC""", (startstasjon, endestasjon, dato, klokkeslett))
    return cursor.fetchall()

def finneTogruterFraStartstasjonTilEndestasjonNesteDag(startstasjon, endestasjon, dato):
    cursor.execute("""SELECT Togforekomst.TogruteID, Startstasjon.JernbanestasjonNavn, Startstasjon.avgangstid, Endestasjon.JernbanestasjonNavn, Endestasjon.ankomsttid, Togforekomst.Dato
                        FROM (((Endestasjon left outer join Startstasjon on Startstasjon.TogruteID == Endestasjon.TogruteID) inner join Togrute on Togrute.TogruteID == Startstasjon.TogruteID) inner join Togforekomst on Togforekomst.TogruteID == Togrute.TogruteID)
                        WHERE Startstasjon.JernbanestasjonNavn = ? and Endestasjon.JernbanestasjonNavn = ? and Togforekomst.Dato = ?
                        ORDER BY Startstasjon.Avgangstid ASC""", (startstasjon, endestasjon, dato))
    return cursor.fetchall()

def finneTogruterFraMellomstasjonTilMellomstasjon(startstasjon, endestasjon, dato, klokkeslett):
    cursor.execute("""SELECT Togforekomst.TogruteID, MStart.JernbanestasjonNavn, MStart.avgangstid, MEnde.JernbanestasjonNavn, MEnde.ankomsttid, Togforekomst.Dato
                    FROM (((Mellomstasjon AS MEnde left outer join Mellomstasjon AS MStart on MStart.TogruteID == MEnde.TogruteID) inner join Togrute on Togrute.TogruteID == MStart.TogruteID) inner join Togforekomst on Togforekomst.TogruteID == Togrute.TogruteID)
                    WHERE MStart.JernbanestasjonNavn = ? and MEnde.JernbanestasjonNavn = ? and Togforekomst.Dato = ? and MStart.Avgangstid <= MEnde.Ankomsttid and MStart.Avgangstid >= ?
                    ORDER BY MStart.Avgangstid ASC""", (startstasjon, endestasjon, dato, klokkeslett))
    return cursor.fetchall()

def finneTogruterFraMellomstasjonTilMellomstasjonNesteDag(startstasjon, endestasjon, dato):
    cursor.execute("""SELECT Togforekomst.TogruteID, MStart.JernbanestasjonNavn, MStart.avgangstid, MEnde.JernbanestasjonNavn, MEnde.ankomsttid, Togforekomst.Dato
                    FROM (((Mellomstasjon AS MEnde left outer join Mellomstasjon AS MStart on MStart.TogruteID == MEnde.TogruteID) inner join Togrute on Togrute.TogruteID == MStart.TogruteID) inner join Togforekomst on Togforekomst.TogruteID == Togrute.TogruteID)
                    WHERE MStart.JernbanestasjonNavn = ? and MEnde.JernbanestasjonNavn = ? and Togforekomst.Dato = ? and MStart.Avgangstid <= MEnde.Ankomsttid
                    ORDER BY MStart.Avgangstid ASC""", (startstasjon, endestasjon, dato))
    return cursor.fetchall()

def finneTogruterFraMellomstasjonTilEndestasjon(startstasjon, endestasjon, dato, klokkeslett):
    cursor.execute("""SELECT Togforekomst.TogruteID, Mellomstasjon.JernbanestasjonNavn, Mellomstasjon.avgangstid, Endestasjon.JernbanestasjonNavn, Endestasjon.ankomsttid, Togforekomst.Dato
                    FROM (((Endestasjon left outer join Mellomstasjon on Mellomstasjon.TogruteID == Endestasjon.TogruteID) inner join Togrute on Togrute.TogruteID == Mellomstasjon.TogruteID) inner join Togforekomst on Togforekomst.TogruteID == Togrute.TogruteID)
                    WHERE Mellomstasjon.JernbanestasjonNavn = ? and Endestasjon.JernbanestasjonNavn = ? and Togforekomst.Dato = ? and Mellomstasjon.Avgangstid >= ?
                    ORDER BY Mellomstasjon.Avgangstid ASC""", (startstasjon, endestasjon, dato, klokkeslett))
    return cursor.fetchall()

def finneTogruterFraMellomstasjonTilEndestasjonNesteDag(startstasjon, endestasjon, dato):
    cursor.execute("""SELECT Togforekomst.TogruteID, Mellomstasjon.JernbanestasjonNavn, Mellomstasjon.avgangstid, Endestasjon.JernbanestasjonNavn, Endestasjon.ankomsttid, Togforekomst.Dato
                    FROM (((Endestasjon left outer join Mellomstasjon on Mellomstasjon.TogruteID == Endestasjon.TogruteID) inner join Togrute on Togrute.TogruteID == Mellomstasjon.TogruteID) inner join Togforekomst on Togforekomst.TogruteID == Togrute.TogruteID)
                    WHERE Mellomstasjon.JernbanestasjonNavn = ? and Endestasjon.JernbanestasjonNavn = ? and Togforekomst.Dato = ?
                    ORDER BY Mellomstasjon.Avgangstid ASC""", (startstasjon, endestasjon, dato))
    return cursor.fetchall()

def convertDato(datoString):
    try:
        dato = dt.strptime(datoString, '%Y-%m-%d').date()
        return dato
        
    except ValueError:
        return None

def convertKlokkeslett(klokkeslettString):
        try:
            klokkeslett = dt.strptime(klokkeslettString, "%H:%M")
            return klokkeslett
        except ValueError:
            return None

def sjekkeGyldigeJernbanestasjoner(jernbanestasjon):
    cursor.execute("SELECT JernbanestasjonNavn FROM Jernbanestasjon")
    gyldige = [item[0] for item in cursor.fetchall()]
    if jernbanestasjon not in gyldige:
        return False
    else:
        return True

sokeitogreiser()

con.close()