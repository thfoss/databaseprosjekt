import sqlite3
from datetime import date 
from datetime import datetime as dt
con = sqlite3.connect("jernbanesystemdb.db") 
cursor = con.cursor()
 
# Registrerte kunder skal kunne finne ledige billetter for en oppgitt strekning på en ønsket togrute
# og kjøpe de billettene hen ønsker.

def billettkjop():
    kundeNr = input("Oppgi ditt kundeNr: ")
    cursor.execute("SELECT KundeNr FROM Kunde WHERE KundeNr = ?", (kundeNr,))
    if len(cursor.fetchall()) == 0:
        print("Du er ikke registrert som kunde. Du må gjøre det før du kan bestille billetter.")
        return

    print("Her er de ulike togrutene som går på Nordlandsbanen:")
    cursor.execute("SELECT * FROM Togrute")
    togruter = cursor.fetchall()
    print("[TogruteID, BanestrekningNavn, VognoppsettID, OperatorNavn, Startstasjon, Endestasjon]")
    for entry in togruter:
        print(entry)

    togruteID = input("Hvilken togrute vil du reise med? Oppgi TogruteID: ")
    while (sjekkeGyldigTogrute(togruteID) == False):
        togruteID = input("Oppgitt togrute er ugyldig. Prøv på nytt: ") 
        
    # Finner alle stasjoner på rute
    cursor.execute("SELECT JernbanestasjonNavn FROM Startstasjon WHERE TogruteID = ?", (togruteID,))
    startstasjonPaaRute = [item[0] for item in cursor.fetchall()]
    cursor.execute("SELECT JernbanestasjonNavn FROM Mellomstasjon WHERE TogruteID = ? ORDER BY Ankomsttid ASC", (togruteID,))
    mellomstasjonerPaaRute = [item[0] for item in cursor.fetchall()]
    cursor.execute("SELECT JernbanestasjonNavn FROM Endestasjon WHERE TogruteID = ?", (togruteID,))
    endestasjonPaaRute = [item[0] for item in cursor.fetchall()]
    stasjonerPaaRute = startstasjonPaaRute + mellomstasjonerPaaRute + endestasjonPaaRute
    print("Dette er jernbanestasjonene i riktig rekkefølge på valgt togrute:", stasjonerPaaRute)
    
    startstasjon = input("Hvilken jernbanestasjon skal reisen starte på?: ")
    while (sjekkeGyldigeJernbanestasjoner(startstasjon) == False):
        startstasjon = input("Oppgitt startstasjon er ugyldig. Prøv på nytt: ")
    endestasjon = input("Hvilken jernbanestasjon skal reisen ende på?: ")
    while (sjekkeGyldigeJernbanestasjoner(endestasjon) == False):
        endestasjon = input("Oppgitt endestasjon er ugyldig. Prøv på nytt: ")
        
    reisedatoString = input("Hvilken dato vil du reise? Format YYYY-MM-DD: ")
    while (convertDato(reisedatoString) == None):
        print("Ugyldig dato, skal være på formatet YYYY-MM-DD.")
        reisedatoString = input("Hvilken dato er reisen? Format YYYY-MM-DD: ")
    
    cursor.execute("SELECT Togforekomst.Dato FROM Togforekomst WHERE TogruteID = ?", (togruteID,))
    gyldigeReisedatoer = [item[0] for item in cursor.fetchall()]
    if (reisedatoString not in gyldigeReisedatoer):
        print("Reisedatoen du har oppgitt er ikke en av de mulige reisedatoene, prøv igjen.")
        return
    reisedato = convertDato(reisedatoString)

    # Finner vognoppsettet til den valgte togruta      
    cursor.execute("SELECT Togrute.VognoppsettID FROM Togrute WHERE TogruteID = ?", (togruteID,))
    vognoppsettID = cursor.fetchone()
    cursor.execute("SELECT AntallSittevogner, AntallSovevogner FROM Vognoppsett WHERE VognoppsettID = ?", (vognoppsettID[0],))
    antallSitteOgSoveIVognoppsett = cursor.fetchall()
    antallSittevogner = int(antallSitteOgSoveIVognoppsett[0][0])
    antallSovevogner = int(antallSitteOgSoveIVognoppsett[0][1])

    #Både sittevogn og sengevogner
    if (antallSovevogner > 0 and antallSittevogner > 0):
        antallSengeBilletter = sjekkeAntallBilletter("senge")
        antallSeteBilletter = sjekkeAntallBilletter("sete")
    #Bare sittevogn
    elif (antallSovevogner == 0 and antallSittevogner > 0):
        antallSeteBilletter = sjekkeAntallBilletter("sete")
        antallSengeBilletter = 0
    #Bare sovevogn
    elif (antallSovevogner > 0 and antallSittevogner == 0):
        antallSengeBilletter = sjekkeAntallBilletter("senge")
        antallSeteBilletter = 0
        
    antallBilletter = antallSeteBilletter + antallSengeBilletter
    
    # Finner vognID til den valgte togruta
    cursor.execute("SELECT Vogn.VognID FROM Vogn INNER JOIN VognerIOppsett ON Vogn.VognID = VognerIOppsett.VognID WHERE VognoppsettID = ?", (vognoppsettID[0],))
    vognIDer = [item[0] for item in cursor.fetchall()]
    cursor.execute("SELECT Sittevogn.VognID FROM Sittevogn")
    sittevognIDer = [item[0] for item in cursor.fetchall()]
    cursor.execute("SELECT Sovevogn.VognID FROM Sovevogn")
    sovevognIDer = [item[0] for item in cursor.fetchall()]

    dictMedAlleSeter = {}
    dictMedAlleSenger = {}
    for vognID in vognIDer:
        if vognID in sittevognIDer:
            # Finner totalt antall seter i en vogn
            antallSeterIVogn = 0
            if (antallSittevogner != 0):
                cursor.execute("SELECT AntallStolrader, AntallSeterPerRad FROM Sittevogn WHERE VognID = ?", (vognID,))
                antallStolraderOgAntallSeterPerRad = cursor.fetchall()
                antallStolrader = int(antallStolraderOgAntallSeterPerRad[0][0])
                antallSeterPerRad = int(antallStolraderOgAntallSeterPerRad[0][1])
                antallSeterIVogn += (antallStolrader * antallSeterPerRad)
                
                listeMedSeterIVogn = listerForSeter(togruteID, vognID, antallSeterIVogn, reisedato)
                dictMedAlleSeter[vognID] = listeMedSeterIVogn
                
        elif vognID in sovevognIDer:
            # Finner totalt antall senger i en vogn
            antallSengerIVogn = 0
            if (antallSovevogner != 0):
                cursor.execute("SELECT AntallSovekupeer FROM Sovevogn WHERE VognID = ?", (vognID,))
                antallSovekupeertuple = cursor.fetchone()
                antallSovekupeer = int(antallSovekupeertuple[0])
                antallSengerIVogn += (antallSovekupeer * 2) 

                listeMedSengerIVogn = listerForSenger(vognID, antallSengerIVogn, reisedato)
                dictMedAlleSenger[vognID] = listeMedSengerIVogn
                
    if (antallSeteBilletter > 0):
        antallSeterTotalt = 0
        for value in dictMedAlleSeter.values():
            antallSeterTotalt = (len(value[0]) * antallSittevogner)
        if (antallSeteBilletter > antallSeterTotalt):
            print("Du har bedt om flere sitteplasser enn det er sitteplasser på toget. Prøv på nytt.")
            return
    
    if (antallSengeBilletter > 0):
        antallSengerTotalt = 0
        for sengevogn in dictMedAlleSenger.values():
            antallSengerTotalt += len(sengevogn)
        antallSengerTotaltTotalt = antallSengerTotalt * antallSovevogner
        if (antallSengeBilletter > antallSengerTotaltTotalt):
            print("Du har bedt om flere sengeplasser enn det er sengeplasser på toget. Prøv på nytt.")
            return
    
    # Bruker indeksen til å finne ut hvor i lista over vi skal begynne å iterere for å finne alle delstrekninger vi skal innom
    indeksTilStartstasjon = stasjonerPaaRute.index(startstasjon)
    indeksTilEndestasjon = stasjonerPaaRute.index(endestasjon)
 
    if (antallSeteBilletter != 0):
        dictMedAlleSeter, tildeltSete, vognIDSete, tildelteSeter = bestilleSeteBilletter(dictMedAlleSeter, antallSeteBilletter, indeksTilStartstasjon, indeksTilEndestasjon)
        if (tildeltSete == None and tildelteSeter == []):
            print("Det er ikke nok ledige plasser på toget. Prøv en annen dato.")
            return
    if (antallSengeBilletter != 0):
        dictMedAlleSenger, tildeltSeng, vognIDSeng, tildelteSenger = bestilleSengeBilletter(dictMedAlleSenger, antallSengeBilletter)
        if (tildeltSeng == None and tildelteSenger == []):
            print("Det er ikke nok ledige plasser på toget. Prøv en annen dato.")
            return
    
    cursor.execute("SELECT COUNT(OrdreNr) FROM Kundeordre")
    antallOrdre = cursor.fetchone() 
    ordrenummer = int(antallOrdre[0]) + 1
    iDag = date.today()
    now = dt.now()
    tidForKjop = now.strftime("%H:%M")
    cursor.execute("INSERT INTO Kundeordre VALUES (?, ?, ?, ?, ?, ?)", (ordrenummer, iDag, tidForKjop, antallBilletter, togruteID, kundeNr))
    
    billettnr = incrementBillettnr()
    if (antallSeteBilletter == 1): # Kun ett sete bestilt
        cursor.execute("INSERT INTO Billett VALUES (?, 'sete', ?, ?, ?, ?, Null, ?, ?)", (billettnr, reisedato, ordrenummer, vognIDSete, tildeltSete, startstasjon, endestasjon))
        con.commit()
    elif (antallSeteBilletter > 1): # Flere seter bestilt
        for sete in tildelteSeter:
            billettnr = incrementBillettnr()
            cursor.execute("INSERT INTO Billett VALUES (?, 'sete', ?, ?, ?, ?, Null, ?, ?)", (billettnr, reisedato, ordrenummer, vognIDSete, sete, startstasjon, endestasjon))
            con.commit()
    
    if (antallSengeBilletter > 0):
        billettnr = incrementBillettnr()
        if (tildeltSeng != None and tildelteSenger == []): # Kun en seng bestilt
            cursor.execute("INSERT INTO Billett VALUES (?, 'seng', ?, ?, ?, Null, ?, ?, ?)", (billettnr, reisedato, ordrenummer, vognIDSeng, tildeltSeng, startstasjon, endestasjon))
            con.commit()
        elif (tildeltSeng != None and tildelteSenger != []): # Oddetall over 1 senger bestilt
            cursor.execute("INSERT INTO Billett VALUES (?, 'seng', ?, ?, ?, Null, ?, ?, ?)", (billettnr, reisedato, ordrenummer, vognIDSeng, tildeltSeng, startstasjon, endestasjon))
            con.commit()
            for seng in tildelteSenger:
                billettnr = incrementBillettnr()
                cursor.execute("INSERT INTO Billett VALUES (?, 'seng', ?, ?, ?, Null, ?, ?, ?)", (billettnr, reisedato, ordrenummer, vognIDSeng, seng, startstasjon, endestasjon))
                con.commit()
        elif (tildeltSeng == None and tildelteSenger != []): # Partall senger bestilt
            for seng in tildelteSenger:
                billettnr = incrementBillettnr()
                cursor.execute("INSERT INTO Billett VALUES (?, 'seng', ?, ?, ?, Null, ?, ?, ?)", (billettnr, reisedato, ordrenummer, vognIDSeng, seng, startstasjon, endestasjon))
                con.commit()

    cursor.execute("""SELECT Billett.BillettID, Billett.Dato, Billett.VognID, Billett.SeteNr, Billett.SengNr, Billett.Startstasjon, Billett.Endestasjon
                        FROM Kundeordre INNER JOIN Billett ON Kundeordre.OrdreNr = Billett.OrdreNr
                        WHERE Kundeordre.KundeNr = ? and Billett.Dato = ? and Kundeordre.TogruteID = ? and Kundeordre.Dato = ?""", (kundeNr, reisedato, togruteID, iDag))
    billettInfo = cursor.fetchall()
    print("Gratulerer, du har gjennomført kjøp! Her er billettene du har bestilt:")
    print("[BillettID, Dato for reise, VognID, SeteNr, SengNr, Startstasjon, Endestasjon]")
    for entry in billettInfo:
        print(entry)


# HJELPEFUNKSJONER
def sjekkeGyldigeJernbanestasjoner(jernbanestasjon):
    cursor.execute("SELECT JernbanestasjonNavn FROM Jernbanestasjon")
    gyldige = [item[0] for item in cursor.fetchall()]
    if jernbanestasjon not in gyldige:
        return False
    else:
        return True
    
def sjekkeGyldigTogrute(togruteID):
    cursor.execute("SELECT TogruteID FROM Togrute")
    gyldige = [item[0] for item in cursor.fetchall()]
    if togruteID.isdigit() == False:
        return False
    elif int(togruteID) not in gyldige:
        return False
    else:
        return True
    
def convertDato(datoString):
    try:
        dato = dt.strptime(datoString, '%Y-%m-%d').date()
        return dato
        
    except ValueError:
        return None
    
def convertKlokkeslett(klokkeslettString):
        try:
            klokkeslett = dt.strptime(klokkeslettString, "%H:%M")
            return klokkeslett
        except ValueError:
            return None
    
def sjekkeAntallBilletter(type):
    antallBilletter = input(f'Hvor mange {type}-plasser ønsker du?: ')
    boolean = False
    while (boolean == False):
        if antallBilletter == "":
            boolean = False
            antallBilletter = input(f'Du oppga antall {type}-billetter som en tom streng. Prøv på nytt: ')
        elif antallBilletter.isdigit() == False:
            boolean = False
            antallBilletter = input(f'Oppgitt antall {type}-billetter er ikke et tall. Prøv på nytt: ')
        elif int(antallBilletter) < 0 or int(antallBilletter) > 500:
            boolean = False
            antallBilletter = input(f'Oppgitt antall {type}-billetter er urimelig. Prøv på nytt: ')
        else:
            boolean = True
    return int(antallBilletter)

def listerForSeter(togruteID, vognID, antallSeterIVogn, reisedato):
    # Henter ut delstrekningene
    cursor.execute("""SELECT Delstrekning.Startstasjon, Delstrekning.Endestasjon
                    FROM Delstrekning join Togrute ON Delstrekning.BanestrekningNavn = Togrute.BanestrekningNavn
                    WHERE Togrute.TogruteID = ?""", (togruteID,))
    delstrekninger = cursor.fetchall()
    
    listeMedLister = []
    for _ in delstrekninger:
        liste = []
        for i in range(antallSeterIVogn):
            liste.insert(i, True)
        listeMedLister.append(liste)
        
    # Henter ut tidligere billetter for å se hvilke seter som allerede er bestilt
    cursor.execute("""SELECT Billett.VognID, Billett.SeteNr, Billett.Startstasjon, Billett.Endestasjon, Billett.Dato, Kundeordre.TogruteID
                   FROM Billett INNER JOIN Kundeordre ON Billett.OrdreNr = Kundeordre.OrdreNr
                   WHERE Billett.Type = 'sete'""")
    billettinfo = cursor.fetchall()
    
    cursor.execute("SELECT JernbanestasjonNavn FROM Startstasjon WHERE TogruteID = ?", (togruteID,))
    startstasjonPaaRute = [item[0] for item in cursor.fetchall()]
    cursor.execute("SELECT JernbanestasjonNavn FROM Mellomstasjon WHERE TogruteID = ? ORDER BY Ankomsttid ASC", (togruteID,))
    mellomstasjonerPaaRute = [item[0] for item in cursor.fetchall()]
    cursor.execute("SELECT JernbanestasjonNavn FROM Endestasjon WHERE TogruteID = ?", (togruteID,))
    endestasjonPaaRute = [item[0] for item in cursor.fetchall()]
    stasjonerPaaRute = startstasjonPaaRute + mellomstasjonerPaaRute + endestasjonPaaRute
    print(stasjonerPaaRute)
    
    # Setter tidligere bestilte seter til False    
    for entry in billettinfo:
        vognIDIBillett = entry[0]
        togruteIDIBillett = entry[5]
        if (int(togruteID) == togruteIDIBillett and vognID == vognIDIBillett):
            seteNrIBillett = entry[1]
            startstasjon = entry[2]
            endestasjon = entry[3]
            reisedatoIBillett = entry[4]
            indekstilstartstasjon = stasjonerPaaRute.index(startstasjon)
            indekstilendestasjon = stasjonerPaaRute.index(endestasjon)
            if (str(reisedato) == reisedatoIBillett):
                if (seteNrIBillett != None):
                    seteNrIndeks = seteNrIBillett - 1
                    for i in range(indekstilstartstasjon, indekstilendestasjon):
                        innerList = listeMedLister[i]
                        innerList[seteNrIndeks] = False
    return listeMedLister

def listerForSenger(vognID, antallSenger, reisedato):
    cursor.execute("SELECT Billett.VognID, Billett.SengNr, Billett.Dato FROM Billett WHERE Billett.Type = 'seng'")
    billettinfo = cursor.fetchall()
    
    liste = []
    for i in range(antallSenger):
        liste.insert(i, True)
    
    for entry in billettinfo:
        vognIDIBillett = entry[0]
        if (vognID == vognIDIBillett):
            sengNrIBillett = entry[1]
            reisedatoIBillett = entry[2]
            if (str(reisedato) == reisedatoIBillett):    
                if (sengNrIBillett != None):
                    sengNrIndeks = sengNrIBillett - 1
                    liste[sengNrIndeks] = False
    
    return liste

def hentElementer(liste, teller): 
    # Lager liste med alle elementene med indeks lik "teller"
    return [item[teller] for item in liste]

def finneLedigSete(listeMedSeter):
        teller = 0
        tildeltSete = None
        for _ in range(len(listeMedSeter)):
            elementer = hentElementer(listeMedSeter, teller)
            if all(elementer):
                tildeltSete = teller + 1
            else:
                teller += 1
        return tildeltSete
    
def bestilleSeteBilletter(dictMedAlleSeter, antallSeteBilletter, indeksTilStartstasjon, indeksTilEndestasjon):
    vognID = 0
    for key in dictMedAlleSeter.keys():
        listeMedSeterIVogn = dictMedAlleSeter.get(key)
        # Modifiserer liste slik at vi bare har med delstrekningene som gjelder
        modifisertListeMedSeter = listeMedSeterIVogn[indeksTilStartstasjon:indeksTilEndestasjon+1]
        
        tildeltSete = None 
        tildelteSeter = [] 
        vognID = key
        if antallSeteBilletter == 1:
            tildeltSete = finneLedigSete(modifisertListeMedSeter)
            for i in range(indeksTilStartstasjon, indeksTilEndestasjon):
                indeksTilTildeltSete = tildeltSete - 1
                listeMedSeterIVogn[i][indeksTilTildeltSete] = False
                dictMedAlleSeter[key] = listeMedSeterIVogn
        elif antallSeteBilletter > 1:
            for _ in range(antallSeteBilletter):
                tildeltSete = finneLedigSete(modifisertListeMedSeter)
                tildelteSeter.append(tildeltSete)
                for i in range(indeksTilStartstasjon, indeksTilEndestasjon):
                    indeksTilTildeltSete = tildeltSete - 1
                    listeMedSeterIVogn[i][indeksTilTildeltSete] = False
                    dictMedAlleSeter[key] = listeMedSeterIVogn
    return dictMedAlleSeter, tildeltSete, vognID, tildelteSeter
    
def finneLedigSeng(listeMedSenger):
    tildeltSeng = None
    for i in range(0, len(listeMedSenger)):
        if (tildeltSeng == None):
            if i%2 == 0:
                if (listeMedSenger[i] == True and listeMedSenger[i+1] == True):
                    # Dersom seng nr. oddetall er ledig OG seng nr. partall er ledig, er hele kupeen ledig og den er ledig for bestilling
                    tildeltSeng = i + 1
    return tildeltSeng

def bestilleSengeBilletter(dictMedAlleSenger, antallSengeBilletter):
    for key in dictMedAlleSenger.keys():
        vognID = key
        listeMedSengerIVogn = dictMedAlleSenger.get(key)
        valgtSeng = None
        listeMedSengNrBestilt = []
        # Partall antall senger
        if (antallSengeBilletter % 2 == 0):
            for _ in range(int(antallSengeBilletter / 2)):
                seng1 = finneLedigSeng(listeMedSengerIVogn)
                seng2 = seng1 + 1
                listeMedSengNrBestilt.append(seng1)
                listeMedSengNrBestilt.append(seng2)
                indeksTilSeng1 = seng1 - 1
                indeksTilSeng2 = seng2 - 1
                listeMedSengerIVogn[indeksTilSeng1] = False
                listeMedSengerIVogn[indeksTilSeng2] = False
                dictMedAlleSenger[key] = listeMedSengerIVogn
        # Oddetall antall senger
        else: 
            if (antallSengeBilletter > 1):
                for _ in range((int((antallSengeBilletter-1)/2)-1)):
                    seng1 = finneLedigSeng(listeMedSengerIVogn)
                    seng2 = seng1 + 1
                    listeMedSengNrBestilt.append(seng1)
                    listeMedSengNrBestilt.append(seng2)
                    listeMedSengerIVogn[seng1-1] = False
                    listeMedSengerIVogn[seng2-1] = False
            valgtSeng = finneLedigSeng(listeMedSengerIVogn)
            indeksTilSeng = valgtSeng - 1
            listeMedSengerIVogn[indeksTilSeng] = False
            dictMedAlleSenger[key] = listeMedSengerIVogn
        return dictMedAlleSenger, valgtSeng, vognID, listeMedSengNrBestilt

def incrementBillettnr():
    cursor.execute("SELECT COUNT(BillettID) FROM Billett")
    antallBilletter = cursor.fetchone() 
    billettnr = int(antallBilletter[0]) + 1
    return billettnr

billettkjop()

con.commit()
con.close()