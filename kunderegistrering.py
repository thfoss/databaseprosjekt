import sqlite3 
import re
con = sqlite3.connect("jernbanesystemdb.db")
cursor = con.cursor()

# En bruker skal kunne registrere seg i kunderegisteret.

def kunderegistrering():
    navn = input("Navn (fornavn og etternavn): ")
    telefonnummer = input("Telefonnummer: ")
    epostadresse = input("E-postadresse: ")
    
    while (validereNavn(navn) != True):
        print("Navnet du skrev inn er ikke riktig formatert, prøv igjen.")
        navn = input("Navn (fornavn og etternavn): ")
        
    while (validereTelefonnummer(telefonnummer) != True):
        print("Telefonnummeret du skrev inn er ikke riktig formatert, prøv igjen.")
        telefonnummer = input("Telefonnummer: ")
        
    while (validereEpost(epostadresse) != True):
        print("Eposten du skrev inn er ikke riktig formatert, prøv igjen.")
        epostadresse = input("E-postadresse: ")    
    
    cursor.execute("SELECT COUNT(KundeNr) FROM Kunde")
    antallKunder = cursor.fetchone()
    kundenummer = int(antallKunder[0]) + 1
    cursor.execute("INSERT INTO Kunde VALUES (?, ?, ?, ?)", (kundenummer, navn, telefonnummer, epostadresse))
    print("Du er nå registrert som kunde. Ditt kundenummer er", kundenummer, ".")
    print("Bruk dette for å bestille billetter og sjekke kundehistorikk senere.")
   
   
# HJELPEFUNKSJONER 
def validereNavn(navn):
    if (navn == "" or len(re.split("\\s+", navn)) != 2 or re.fullmatch("^[A-ZÆØÅ][a-zæøå]+\s[A-ZÆØÅ][a-zæøå]+$", navn) == False):
        return False
    
    for delAvNavn in re.split("\\s+", navn):
        if (len(delAvNavn) < 2):
            return False
    return True

def validereTelefonnummer(telefonnummer):
    tlfUtenMellomrom = telefonnummer.replace(" ", "")
    if len(tlfUtenMellomrom) > 8:
        if tlfUtenMellomrom.startswith("+479") or tlfUtenMellomrom.startswith("+474"):
            if len(tlfUtenMellomrom) == 11:
                return True
            
    elif len(tlfUtenMellomrom) == 8:
        if tlfUtenMellomrom.startswith("9") or tlfUtenMellomrom.startswith("4"):
            return True
        
    else:
        return False

# Antar e-postaddresse på formen navn@navnesen.landkode
def validereEpost(epostadresse):
    landskoder = ["ad", "ae", "af", "ag", "ai", "al", "am", "ao", "aq", 
    "ar", "as", "at", "au", "aw", "ax", "az", "ba", "bb", "bd", "be", "bf", "bg", "bh", "bi", "bj", "bl", "bm", 
    "bn", "bo", "bq", "br", "bs", "bt", "bv", "bw", "by", "bz", "ca", "cc", "cd", "cf", "cg", "ch", "ci", "ck", 
    "cl", "cm", "cn", "co", "cr", "cu", "cv", "cw", "cx", "cy", "cz", "de", "dj", "dk", "dm", "do", "dz", "ec", 
    "ee", "eg", "eh", "er", "es", "et", "fi", "fj", "fk", "fm", "fo", "fr", "ga", "gb", "gd", "ge", "gf", "gg", 
    "gh", "gi", "gl", "gm", "gn", "gp", "gq", "gr", "gs", "gt", "gu", "gw", "gy", "hk", "hm", "hn", "hr", "ht", 
    "hu", "id", "ie", "il", "im", "in", "io", "iq", "ir", "is", "it", "je", "jm", "jo", "jp", "ke", "kg", "kh", 
    "ki", "km", "kn", "kp", "kr", "kw", "ky", "kz", "la", "lb", "lc", "li", "lk", "lr", "ls", "lt", "lu", "lv", 
    "ly", "ma", "mc", "md", "me", "mf", "mg", "mh", "mk", "ml", "mm", "mn", "mo", "mp", "mq", "mr", "ms", "mt", 
    "mu", "mv", "mw", "mx", "my", "mz", "na", "nc", "ne", "nf", "ng", "ni", "nl", "no", "np", "nr", "nu", "nz", 
    "om", "pa", "pe", "pf", "pg", "ph", "pk", "pl", "pm", "pn", "pr", "ps", "pt", "pw", "py", "qa", "re", "ro", 
    "rs", "ru", "rw", "sa", "sb", "sc", "sd", "se", "sg", "sh", "si", "sj", "sk", "sl", "sm", "sn", "so", "sr", 
    "ss", "st", "sv", "sx", "sy", "sz", "tc", "td", "tf", "tg", "th", "tj", "tk", "tl", "tm", "tn", "to", "tr", 
    "tt", "tv", "tw", "tz", "ua", "ug", "um", "us", "uy", "uz", "va", "vc", "ve", "vg", "vi", "vn", "vu", "wf", 
    "ws", "ye", "yt", "za", "zm", "zw"]
    
    # Her har vi bare sagt at dette formatet er gyldig, men den vil f.eks. ikke godta en @stud.ntnu.no e-post.
    # Vi valgte å ikke bruke noe mer tid på dette ettersom e-post-validering ikke er fokus i prosjektet.
    if re.fullmatch("[a-z]{2,}[@][a-z0-9]{1,}[.][a-z]{2}", epostadresse.lower()):
        splitEpost = re.split("[\\W]", epostadresse)
        landskode = splitEpost[2]
        
        if (epostadresse != "" and landskoder.count(landskode) > 0):
            return True
        return False
    return False

kunderegistrering()

con.commit()
con.close()