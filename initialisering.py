import sqlite3
con = sqlite3.connect("jernbanesystemdb.db") 
cursor = con.cursor()

try:
    cursor.execute("""CREATE TABLE Banestrekning(BanestrekningNavn text NOT NULL, Fremdriftsenergi text NOT NULL, AntallDelstrekninger integer NOT NULL, Hovedretning text NOT NULL, OperatorNavn text NOT NULL, Startstasjon text NOT NULL, Endestasjon text NOT NULL, 
                PRIMARY KEY (BanestrekningNavn), 
                FOREIGN KEY(OperatorNavn) REFERENCES Operator(OperatorNavn) 
                    ON UPDATE CASCADE 
                    ON DELETE NO ACTION, 
                FOREIGN KEY(Startstasjon) REFERENCES Jernbanestasjon(JernbanestasjonNavn) 
                    ON UPDATE CASCADE 
                    ON DELETE NO ACTION, 
                FOREIGN KEY(Endestasjon) REFERENCES Jernbanestasjon(JernbanestasjonNavn) 
                    ON UPDATE CASCADE 
                    ON DELETE NO ACTION)""")
    cursor.execute("""CREATE TABLE Delstrekning(DelstrekningID integer NOT NULL, Lengde integer NOT NULL, AntallSpor integer NOT NULL, BanestrekningNavn text NOT NULL, Startstasjon text NOT NULL, Endestasjon text NOT NULL, 
                PRIMARY KEY(DelstrekningID), 
                FOREIGN KEY(BanestrekningNavn) REFERENCES Banestrekning(BanestrekningNavn) 
                    ON UPDATE CASCADE 
                    ON DELETE NO ACTION, 
                FOREIGN KEY(Startstasjon) REFERENCES Jernbanestasjon(JernbanestasjonNavn) 
                    ON UPDATE CASCADE 
                    ON DELETE NO ACTION, 
                FOREIGN KEY(Endestasjon) REFERENCES Jernbanestasjon(JernbanestasjonNavn) 
                    ON UPDATE CASCADE 
                    ON DELETE NO ACTION)""")
    cursor.execute("""CREATE TABLE Jernbanestasjon(JernbanestasjonNavn text NOT NULL, AntallHoydemeter integer NOT NULL, 
                PRIMARY KEY(JernbanestasjonNavn))""")
    cursor.execute("""CREATE TABLE Togrute(TogruteID integer NOT NULL, BanestrekningNavn text NOT NULL, VognoppsettID integer NOT NULL, OperatorNavn text NOT NULL, Startstasjon text NOT NULL, Endestasjon text NOT NULL, 
                PRIMARY KEY(TogruteID), 
                FOREIGN KEY(BanestrekningNavn) REFERENCES Banestrekning(BanestrekningNavn) 
                    ON UPDATE CASCADE 
                    ON DELETE NO ACTION, 
                FOREIGN KEY(VognoppsettID) REFERENCES Vognoppsett(VognoppsettID) 
                    ON UPDATE CASCADE 
                    ON DELETE NO ACTION, 
                FOREIGN KEY(Startstasjon) REFERENCES Jernbanestasjon(JernbanestasjonNavn) 
                    ON UPDATE CASCADE 
                    ON DELETE NO ACTION, 
                FOREIGN KEY(Endestasjon) REFERENCES Jernbanestasjon(JernbanestasjonNavn) 
                    ON UPDATE CASCADE 
                    ON DELETE NO ACTION, 
                FOREIGN KEY(OperatorNavn) REFERENCES Operator(OperatorNavn) 
                    ON UPDATE CASCADE 
                    ON DELETE NO ACTION)""")
    cursor.execute("""CREATE TABLE Togforekomst(TogruteID integer NOT NULL, Dag text NOT NULL, Dato date NOT NULL,
                PRIMARY KEY(TogruteID, Dag), 
                FOREIGN KEY(TogruteID) REFERENCES Togrute(TogruteID) 
                    ON UPDATE CASCADE 
                    ON DELETE NO ACTION)""")
    cursor.execute("""CREATE TABLE Startstasjon(JernbanestasjonNavn text NOT NULL, TogruteID integer NOT NULL, Avgangstid time NOT NULL, 
                PRIMARY KEY(JernbanestasjonNavn, TogruteID) , 
                FOREIGN KEY(JernbanestasjonNavn) REFERENCES Jernbanestasjon(JernbanestasjonNavn) 
                    ON UPDATE CASCADE 
                    ON DELETE NO ACTION)""")
    cursor.execute("""CREATE TABLE Endestasjon(JernbanestasjonNavn text NOT NULL, TogruteID integer NOT NULL, Ankomsttid time NOT NULL, 
                PRIMARY KEY(JernbanestasjonNavn, TogruteID), 
                FOREIGN KEY(JernbanestasjonNavn) REFERENCES Jernbanestasjon(JernbanestasjonNavn) 
                    ON UPDATE CASCADE 
                    ON DELETE NO ACTION)""")
    cursor.execute("""CREATE TABLE Mellomstasjon(JernbanestasjonNavn text NOT NULL, TogruteID integer NOT NULL, Ankomsttid time NOT NULL, Avgangstid time NOT NULL, 
                PRIMARY KEY(JernbanestasjonNavn, TogruteID), 
                FOREIGN KEY(JernbanestasjonNavn) REFERENCES Jernbanestasjon(JernbanestasjonNavn) 
                    ON UPDATE CASCADE 
                    ON DELETE NO ACTION)""")
    cursor.execute("""CREATE TABLE Operator(OperatorNavn text NOT NULL, AntallVogntyper integer NOT NULL, 
                PRIMARY KEY(OperatorNavn))""")
    cursor.execute("""CREATE TABLE Kunde(KundeNr text NOT NULL, Navn text NOT NULL, Epostadresse text NOT NULL, Telefonnummer integer NOT NULL, 
                PRIMARY KEY(KundeNr))""")
    cursor.execute("""CREATE TABLE Kundeordre(OrdreNr integer NOT NULL, Dato date NOT NULL, Tid time NOT NULL, AntallBilletter integer NOT NULL, TogruteID integer NOT NULL, KundeNr integer NOT NULL, 
                PRIMARY KEY(OrdreNr), 
                FOREIGN KEY(TogruteID) REFERENCES Togrute(TogruteID) 
                    ON UPDATE CASCADE 
                    ON DELETE NO ACTION, 
                FOREIGN KEY(KundeNr) REFERENCES Kunde(KundeNr) 
                    ON UPDATE CASCADE 
                    ON DELETE NO ACTION)""")
    cursor.execute("""CREATE TABLE Vognoppsett(VognoppsettID integer NOT NULL, AntallSovevogner integer NOT NULL, AntallSittevogner integer NOT NULL, 
                PRIMARY KEY(VognoppsettID))""")
    cursor.execute("""CREATE TABLE VognerIOppsett(VognID integer NOT NULL, VognoppsettID integer NOT NULL, 
                PRIMARY KEY(VognID, VognoppsettID), 
                FOREIGN KEY(VognID) REFERENCES Vogn(VognID) 
                    ON UPDATE CASCADE
                    ON DELETE NO ACTION)""")
    cursor.execute("""CREATE TABLE Vogn(VognID integer NOT NULL, VognTypeNavn text NOT NULL, 
                PRIMARY KEY(VognID))""")
    cursor.execute("""CREATE TABLE Sittevogn(VognID integer NOT NULL, AntallStolrader integer NOT NULL, AntallSeterPerRad integer NOT NULL, 
                PRIMARY KEY(VognID))""")
    cursor.execute("""CREATE TABLE Sovevogn(VognID integer NOT NULL, AntallSovekupeer integer NOT NULL, 
                PRIMARY KEY(VognID))""")
    cursor.execute("""CREATE TABLE Sete(VognID integer NOT NULL, SeteNr integer NOT NULL, 
                PRIMARY KEY(VognID, SeteNr), 
                FOREIGN KEY(VognID) REFERENCES Vogn(VognID) 
                    ON UPDATE CASCADE 
                    ON DELETE NO ACTION)""")
    cursor.execute("""CREATE TABLE Seng(VognID integer NOT NULL, Sengnr text NOT NULL, 
                PRIMARY KEY(VognID, SengNr), 
                FOREIGN KEY(VognID) REFERENCES Vogn(VognID) 
                    ON UPDATE CASCADE 
                    ON DELETE NO ACTION)""")
    cursor.execute("""CREATE TABLE Billett(BillettID text NOT NULL, Type text NOT NULL, Dato text NOT NULL, OrdreNr integer NOT NULL, VognID integer NOT NULL, SeteNr integer, SengNr integer, Startstasjon text NOT NULL, Endestasjon text NOT NULL,
                PRIMARY KEY(BillettID), 
                FOREIGN KEY(OrdreNr) REFERENCES KundeOrdre(OrdreNr)
                    ON UPDATE CASCADE 
                    ON DELETE NO ACTION, 
                FOREIGN KEY(VognID, SeteNr) REFERENCES Sete(VognID, SeteNr) 
                    ON UPDATE CASCADE 
                    ON DELETE NO ACTION, 
                FOREIGN KEY(VognID, SengNr) REFERENCES Seng(VognID, SengNr) 
                    ON UPDATE CASCADE 
                    ON DELETE NO ACTION,
                FOREIGN KEY(Startstasjon) REFERENCES Startstasjon(JernbanestasjonNavn)
                    ON UPDATE CASCADE 
                    ON DELETE NO ACTION,
                FOREIGN KEY(Endestasjon) REFERENCES Endestasjon(JernbanestasjonNavn) 
                    ON UPDATE CASCADE 
                    ON DELETE NO ACTION)""")

    # Legge inn data i tabeller
    cursor.execute("INSERT INTO Operator VALUES ('SJ', 2)")
    # Definere banestrekning
    cursor.execute("INSERT INTO Jernbanestasjon VALUES ('Trondheim S', 5.1)")
    cursor.execute("INSERT INTO Jernbanestasjon VALUES ('Steinkjer', 3.6)")
    cursor.execute("INSERT INTO Jernbanestasjon VALUES ('Mosjøen', 6.8)")
    cursor.execute("INSERT INTO Jernbanestasjon VALUES ('Mo i Rana', 3.5)")
    cursor.execute("INSERT INTO Jernbanestasjon VALUES ('Fauske', 34.0)")
    cursor.execute("INSERT INTO Jernbanestasjon VALUES ('Bodø', 4.1)")
    cursor.execute("INSERT INTO Banestrekning VALUES ('Nordlandsbanen', 'Diesel', 5, 'Nord', 'SJ', 'Trondheim S', 'Bodø')")
    cursor.execute("INSERT INTO Delstrekning VALUES (1, 120, 2, 'Nordlandsbanen', 'Trondheim S', 'Steinkjer')")
    cursor.execute("INSERT INTO Delstrekning VALUES (2, 280, 1, 'Nordlandsbanen', 'Steinkjer', 'Mosjøen')")
    cursor.execute("INSERT INTO Delstrekning VALUES (3, 90, 1, 'Nordlandsbanen', 'Mosjøen', 'Mo i Rana')")
    cursor.execute("INSERT INTO Delstrekning VALUES (4, 170, 1, 'Nordlandsbanen', 'Mo i Rana', 'Fauske')")
    cursor.execute("INSERT INTO Delstrekning VALUES (5, 60, 1, 'Nordlandsbanen', 'Fauske', 'Bodø')")

    # Dagtog fra Trondheim til Bodø
    # Vognoppsett dagtog
    cursor.execute("INSERT INTO Vogn VALUES (1, 'SJ-sittevogn-1')")
    cursor.execute("INSERT INTO Vogn VALUES (2, 'SJ-sittevogn-1')")
    cursor.execute("INSERT INTO Sittevogn VALUES (1, 3, 4)")
    cursor.execute("INSERT INTO Sittevogn VALUES (2, 3, 4)")
    cursor.execute("INSERT INTO Sete VALUES (1, 1)")
    cursor.execute("INSERT INTO Sete VALUES (1, 2)")
    cursor.execute("INSERT INTO Sete VALUES (1, 3)")
    cursor.execute("INSERT INTO Sete VALUES (1, 4)")
    cursor.execute("INSERT INTO Sete VALUES (1, 5)")
    cursor.execute("INSERT INTO Sete VALUES (1, 6)")
    cursor.execute("INSERT INTO Sete VALUES (1, 7)")
    cursor.execute("INSERT INTO Sete VALUES (1, 8)")
    cursor.execute("INSERT INTO Sete VALUES (1, 9)")
    cursor.execute("INSERT INTO Sete VALUES (1, 10)")
    cursor.execute("INSERT INTO Sete VALUES (1, 11)")
    cursor.execute("INSERT INTO Sete VALUES (1, 12)")
    cursor.execute("INSERT INTO Sete VALUES (2, 1)")
    cursor.execute("INSERT INTO Sete VALUES (2, 2)")
    cursor.execute("INSERT INTO Sete VALUES (2, 3)")
    cursor.execute("INSERT INTO Sete VALUES (2, 4)")
    cursor.execute("INSERT INTO Sete VALUES (2, 5)")
    cursor.execute("INSERT INTO Sete VALUES (2, 6)")
    cursor.execute("INSERT INTO Sete VALUES (2, 7)")
    cursor.execute("INSERT INTO Sete VALUES (2, 8)")
    cursor.execute("INSERT INTO Sete VALUES (2, 9)")
    cursor.execute("INSERT INTO Sete VALUES (2, 10)")
    cursor.execute("INSERT INTO Sete VALUES (2, 11)")
    cursor.execute("INSERT INTO Sete VALUES (2, 12)")
    cursor.execute("INSERT INTO Vognoppsett VALUES (1, 0, 2)")
    cursor.execute("INSERT INTO VognerIOppsett VALUES (1, 1)")
    cursor.execute("INSERT INTO VognerIOppsett VALUES (2, 1)")
    # Togrutetabell
    cursor.execute("INSERT INTO Togrute VALUES (1, 'Nordlandsbanen', 1, 'SJ', 'Trondheim S', 'Bodø')")
    cursor.execute("INSERT INTO Togforekomst VALUES (1, 'Mandag', '2023-04-03')")
    cursor.execute("INSERT INTO Togforekomst VALUES (1, 'Tirsdag', '2023-04-04')")
    cursor.execute("INSERT INTO Togforekomst VALUES (1, 'Onsdag', '2023-04-05')")
    cursor.execute("INSERT INTO Togforekomst VALUES (1, 'Torsdag', '2023-04-06')")
    cursor.execute("INSERT INTO Togforekomst VALUES (1, 'Fredag', '2023-04-07')")

    cursor.execute("INSERT INTO Startstasjon VALUES ('Trondheim S', 1, '07:49')")
    cursor.execute("INSERT INTO Mellomstasjon VALUES ('Steinkjer', 1, '09:51', '09:51')")
    cursor.execute("INSERT INTO Mellomstasjon VALUES ('Mosjøen', 1, '13:20', '13:20')")
    cursor.execute("INSERT INTO Mellomstasjon VALUES ('Mo i Rana', 1, '14:31', '14:31')")
    cursor.execute("INSERT INTO Mellomstasjon VALUES ('Fauske', 1, '16:49', '16:49')")
    cursor.execute("INSERT INTO Endestasjon VALUES ('Bodø', 1, '17:34')")

    # Nattog fra Trondheim til Bodø
    # Vognoppsett nattog
    cursor.execute("INSERT INTO Vogn VALUES (3, 'SJ-sovevogn-1')")
    cursor.execute("INSERT INTO Sovevogn VALUES (3, 4)")
    cursor.execute("INSERT INTO Seng VALUES (3, 1)")
    cursor.execute("INSERT INTO Seng VALUES (3, 2)")
    cursor.execute("INSERT INTO Seng VALUES (3, 3)")
    cursor.execute("INSERT INTO Seng VALUES (3, 4)")
    cursor.execute("INSERT INTO Seng VALUES (3, 5)")
    cursor.execute("INSERT INTO Seng VALUES (3, 6)")
    cursor.execute("INSERT INTO Seng VALUES (3, 7)")
    cursor.execute("INSERT INTO Seng VALUES (3, 8)")
    cursor.execute("INSERT INTO Vognoppsett VALUES (2, 1, 1)")
    cursor.execute("INSERT INTO VognerIOppsett VALUES (1, 2)")
    cursor.execute("INSERT INTO VognerIOppsett VALUES (3, 2)")
    # Togrutetabell
    cursor.execute("INSERT INTO Togrute VALUES (2, 'Nordlandsbanen', 2, 'SJ', 'Trondheim S', 'Bodø')")
    cursor.execute("INSERT INTO Togforekomst VALUES (2, 'Mandag', '2023-04-03')")
    cursor.execute("INSERT INTO Togforekomst VALUES (2, 'Tirsdag', '2023-04-04')")
    cursor.execute("INSERT INTO Togforekomst VALUES (2, 'Onsdag', '2023-04-05')")
    cursor.execute("INSERT INTO Togforekomst VALUES (2, 'Torsdag', '2023-04-06')")
    cursor.execute("INSERT INTO Togforekomst VALUES (2, 'Fredag', '2023-04-07')")
    cursor.execute("INSERT INTO Togforekomst VALUES (2, 'Lørdag', '2023-04-08')")
    cursor.execute("INSERT INTO Togforekomst VALUES (2, 'Søndag', '2023-04-09')")

    cursor.execute("INSERT INTO Startstasjon VALUES ('Trondheim S', 2, '23:05')")
    cursor.execute("INSERT INTO Mellomstasjon VALUES ('Steinkjer', 2, '00:57', '00:57')")
    cursor.execute("INSERT INTO Mellomstasjon VALUES ('Mosjøen', 2, '04:41', '04:41')")
    cursor.execute("INSERT INTO Mellomstasjon VALUES ('Mo i Rana', 2, '05:55', '05:55')")
    cursor.execute("INSERT INTO Mellomstasjon VALUES ('Fauske', 2, '08:19', '08:19')")
    cursor.execute("INSERT INTO Endestasjon VALUES ('Bodø', 2, '09:05')")

    # Morgentog fra Mo i Rana til Trondheim
    # Vognoppsett morgentog
    cursor.execute("INSERT INTO Vognoppsett VALUES (3, 0, 1)")
    cursor.execute("INSERT INTO VognerIOppsett VALUES (1, 3)")
    # Togrutetabell
    cursor.execute("INSERT INTO Togrute VALUES (3, 'Nordlandsbanen', 3, 'SJ', 'Mo i Rana', 'Trondheim S')")
    cursor.execute("INSERT INTO Togforekomst VALUES (3, 'Mandag', '2023-04-03')")
    cursor.execute("INSERT INTO Togforekomst VALUES (3, 'Tirsdag', '2023-04-04')")
    cursor.execute("INSERT INTO Togforekomst VALUES (3, 'Onsdag', '2023-04-05')")
    cursor.execute("INSERT INTO Togforekomst VALUES (3, 'Torsdag', '2023-04-06')")
    cursor.execute("INSERT INTO Togforekomst VALUES (3, 'Fredag', '2023-04-07')")

    cursor.execute("INSERT INTO Startstasjon VALUES ('Mo i Rana', 3, '08:11')")
    cursor.execute("INSERT INTO Mellomstasjon VALUES ('Mosjøen', 3, '09:14', '09:14')")
    cursor.execute("INSERT INTO Mellomstasjon VALUES ('Steinkjer', 3, '12:31', '12:31')")
    cursor.execute("INSERT INTO Endestasjon VALUES ('Trondheim S', 3, '14:13')")
    
    print("Initialiseringen var vellykket.")
    
except:
    print("Noe gikk galt.")

con.commit()
con.close()