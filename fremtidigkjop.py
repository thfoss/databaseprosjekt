import sqlite3
from datetime import date
from datetime import datetime as dt
con = sqlite3.connect("jernbanesystemdb.db")
cursor = con.cursor()

# For en bruker skal man kunne finne all informasjon om de kjøpene hen har gjort for fremtidige reiser.

def fremtidigkjop():
    kundeNr = input("Skriv inn kundenummeret ditt for å få tilgang til ordrene dine: ")
    cursor.execute("SELECT KundeNr FROM Kunde")
    brukteKundeNr = [item[0] for item in cursor.fetchall()]
    while (kundeNr not in brukteKundeNr):
        print("Ikke et gyldig kundenummer. Vennligst prøv igjen.")
        kundeNr = input("Skriv inn kundenummeret ditt for å få tilgang til ordrene dine: ")
        
    idag = date.today()
    tidNaa = dt.now().strftime("%H:%M")
    
    # Finne startstasjonene til ordrene til kunden:
    cursor.execute("""SELECT Billett.Startstasjon
                    FROM Kundeordre INNER JOIN Billett ON Kundeordre.OrdreNr == Billett.OrdreNr 
                    WHERE Kundeordre.KundeNr = ? and Kundeordre.Dato >=  ?""", (kundeNr, idag))
    kundeordre = cursor.fetchall()
    for i in kundeordre:
        # Sjekker om startstasjon i ordre er av typen startstasjon, endestasjon eller mellomstasjon
        cursor.execute("SELECT JernbanestasjonNavn FROM Startstasjon WHERE JernbanestasjonNavn = ?", (i[0],))
        startstasjon = [item[0] for item in cursor.fetchall()]
        cursor.execute("SELECT JernbanestasjonNavn FROM Endestasjon WHERE JernbanestasjonNavn = ?", (i[0],))
        endestasjon = [item[0] for item in cursor.fetchall()]
        cursor.execute("SELECT JernbanestasjonNavn FROM Mellomstasjon WHERE JernbanestasjonNavn = ?", (i[0],))
        mellomstasjon = [item[0] for item in cursor.fetchall()]
        if len(startstasjon) > 0:
            cursor.execute("""SELECT DISTINCT Kundeordre.OrdreNr, Kundeordre.Dato, Kundeordre.Tid, Kundeordre.AntallBilletter, Kundeordre.TogruteID, Startstasjon.JernbanestasjonNavn
                              FROM (Kundeordre inner join Billett ON Kundeordre.OrdreNr = Billett.OrdreNr) inner join Startstasjon ON Billett.Startstasjon = Startstasjon.JernbanestasjonNavn
                              WHERE Startstasjon >= ?""", (tidNaa,))
    
        elif len(mellomstasjon) > 0:
            cursor.execute("""SELECT DISTINCT Kundeordre.OrdreNr, Kundeordre.Dato, Kundeordre.Tid, Kundeordre.AntallBilletter, Kundeordre.TogruteID, Startstasjon.JernbanestasjonNavn
                              FROM (Kundeordre inner join Billett ON Kundeordre.OrdreNr = Billett.OrdreNr) inner join Startstasjon ON Billett.Startstasjon = Startstasjon.JernbanestasjonNavn
                              WHERE Startstasjon >= ?""", (tidNaa,))
        
    
    fremtidigOrdre = cursor.fetchall()
    print("Her er alle dine fremtidige kjøp:")
    print("[OrdreNr, Dato for kjøp, Tid for kjøp, Antall billetter, TogruteID, Startstasjon]")
    for entry in fremtidigOrdre:
        print(entry)

fremtidigkjop()

con.commit()
con.close()