# Databaseprosjekt - Del 2

## Beskrivelse av innlevering
Dette er besvarelsen til gruppe 220 for databaseprosjektet del 2, database over jernbanesystemet i Norge.
Vi har en tom databasefil der all informasjonen vil bli lagret i denne mappen under navnet jernbanesystemdb.db. For å initialisere denne og fylle databasen med den nødvendige startinformasjonen, MÅ python-filen initialisering.py kjøres først! Videre ligger også sql-filen i sin helhet, et bilde med en revidert utgave av ER-modellen, samt en rekke andre python-filer i samme mappe.

Vi har strukturert oppgaven slik at vi har en .py-fil for hver handling en bruker kan benytte seg av, altså:
- hentetogreiser.py håndterer at en bruker kan hente ut alle togreiser som er innom en gitt jernbanestasjon en gitt ukedag
- sokeitogreiser.py håndterer at en bruker kan søke etter togreiser mellom to jernbanestasjoner fra et angitt tidspunkt og dato og inkluderer avreisene neste dag
- kunderegistrering.py håndtering at en bruker kan registrere seg i kunderegisteret
- billett.py håndterer at en bruker kan hente ut ledige billetter og kjøpe billetter til en ønsket togrute
- fremtidigkjop.py håndterer at en bruker kan hente ut denne kundens fremtidige kjøp

## Antagelser
- I brukerhistorie D antar vi at klokkeslettet brukeren oppgir er avgangstid fra startstasjonen. I tillegg står det at "Alle ruter den samme dagen og den neste skal returneres, sortert på tid". Vi antar at dette gjelder fra og med det oppgitte tidspunktet. Eks. hvis brukeren søker etter tog som går fra og med kl 16, så skal ikke et tog som går klokken 13 samme dag returneres. Men siden vi tolker det oppgitte tidspunktet for dagen som oppgis, skal alle ruter den neste dagen returneres.
- Delstrekninger må legges inn i tabellen i rekkefølgen toget passerer de for at koden skal fungere
- I filen for billettkjøp antar vi at en kunde vet hvilken stasjon hen ønsker å starte/slutte turen på, og at hen ut i fra togrutens start- og endestasjon skjønner hvilken rute (togruteID) hen må ta. 
- I billett.py har vi valgt å ikke validere om personen legger inn start- og endestasjon i riktig rekkefølge (altså kan hen legge inn Bodø som startstasjon og Trondheim som endestasjon selv om toget går andre veien). Valgte bort denne valideringen for å kunne fokusere mer på det som er relevant med tanke på pensum. Har derimot skrevet ut stasjonene på ruta i riktig rekkefølge, slik at brukeren skal forstå hvilke jernbanestasjoner hen kan velge. 

## Endringer vi har gjort på del 1
- Endrer datatypen til Dato i Kundeordre til string i stedet for integer
- Endrer datatypen til Tid i Kundeordre til time i stedet for integer
- Legger til start og endestasjon i Billett
- Fjernet NOT NULL restriksjon fra SeteNr og SengNr i Billett
- Vi har endret entitetsklasse som heter VognType til å hete Vogn. Dette måtte vi gjøre for å klare å definere et unikt sete. Eks. hvis et tog besto av flere identiske vogner var det ikke mulig å skille setene fra hverandre. Vi har lagt til VognID som attributt for å løse dette. Ergo er en vogn bestemt av en VognID og VognTypeNavn. Vedlagt i mappen ligger en .png-fil med det nye ER-diagrammet under navnet "ER modell, revidert".


## Kilder
https://stackoverflow.com/questions/12867140/python-mysqldb-get-the-result-of-fetchall-in-a-list
- Brukt til inspirasjon for hvordan man kan hente ut resultatene fra cursor.fetchall() til en liste