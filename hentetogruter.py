import sqlite3
con = sqlite3.connect("jernbanesystemdb.db")
cursor = con.cursor() 

# For en stasjon som oppgis, skal bruker få ut alle togruter som er innom stasjonen en gitt ukedag.

def hentetogruter():
    # Henter JernbanestasjonNavn fra DB og sjekker samtidig at det er en gyldig input
    jernbanestasjon = input("Hvilken jernbanestasjon er du ute etter? ")
    while (sjekkeGyldigeJernbanestasjoner(jernbanestasjon) == False):
        jernbanestasjon = input("Oppgitt jernbanestasjon er ugyldig. Prøv på nytt: ")
    
    # Sjekker om oppgitt JernbanestasjonNavn er en startstasjon, endestasjon eller mellomstasjon
    cursor.execute("SELECT JernbanestasjonNavn FROM Startstasjon WHERE JernbanestasjonNavn = ?", (jernbanestasjon,))
    startstasjon = [item[0] for item in cursor.fetchall()]
    cursor.execute("SELECT JernbanestasjonNavn FROM Endestasjon WHERE JernbanestasjonNavn = ?", (jernbanestasjon,))
    endestasjon = [item[0] for item in cursor.fetchall()]
    cursor.execute("SELECT JernbanestasjonNavn FROM Mellomstasjon WHERE JernbanestasjonNavn = ?", (jernbanestasjon,))
    mellomstasjon = [item[0] for item in cursor.fetchall()]
    
    # Filtrer på dag
    gyldigeDager = ["mandag", "tirsdag", "onsdag", "torsdag", "fredag", "lørdag", "søndag"]
    dag = input("Hvilken ukedag ønsker du å reise? ")
    while dag.lower() not in gyldigeDager:
        dag = input("Ugyldig dag oppgitt. Prøv på nytt: ")        
    
    
    # Edge-cases der den oppgitte jernbanestasjonen er flere typer stasjoner
    # Jernbanestasjon er startstasjon OG mellomstasjon
    if jernbanestasjon in startstasjon and mellomstasjon:
        startstasjonavganger = finneStartstasjonavganger(dag, jernbanestasjon)
        mellomstasjonavganger = finneMellomstasjonavganger(dag, jernbanestasjon)
        togruter = startstasjonavganger + mellomstasjonavganger
    
    # Jernbanestasjon er mellomstasjon OG endestasjon
    elif jernbanestasjon in mellomstasjon and endestasjon:
        mellomstasjonavganger = finneEndestasjonavganger(dag, jernbanestasjon)
        endestasjonavganger = finneMellomstasjonavganger(dag, jernbanestasjon)
        togruter = mellomstasjonavganger + endestasjonavganger
    
    # Jernbanestasjon er startstasjon OG endestasjon
    elif jernbanestasjon in startstasjon and endestasjon: 
        startstasjonavganger = finneStartstasjonavganger(dag, jernbanestasjon)
        endestasjonavganger = finneEndestasjonavganger(dag, jernbanestasjon)
        togruter = startstasjonavganger + endestasjonavganger
    
    # Jernbanestasjon er startstasjon OG endestasjon OG mellomstasjon
    elif jernbanestasjon in startstasjon and endestasjon and mellomstasjon: 
        startstasjonavganger = finneStartstasjonavganger(dag, jernbanestasjon)
        endestasjonavganger = finneEndestasjonavganger(dag, jernbanestasjon)
        mellomstasjonavganger = finneMellomstasjonavganger(dag, jernbanestasjon)
        togruter = startstasjonavganger + endestasjonavganger + mellomstasjonavganger
    
    else:
        if len(startstasjon) > 0:
            # Den oppgitte jernbanestasjonen er en startstasjon
            togruter = finneStartstasjonavganger(dag, jernbanestasjon)
            
        elif len(endestasjon) > 0:
            # Den oppgitte jernbanestasjonen er en endestasjon
            togruter = finneEndestasjonavganger(dag, jernbanestasjon)
        else:
            # Den oppgitte jernbanestasjonen er en mellomstasjon
            togruter = finneMellomstasjonavganger(dag, jernbanestasjon)
            
    print("Her er togrutene som er innom jernbanestasjonen på denne dagen:")
    print("[Dag, TogruteID, Avgangstid, Destinasjon]")
    for entry in togruter:
        print(entry)
    
    
# HJELPEFUNKSJONER  
def sjekkeGyldigeJernbanestasjoner(jernbanestasjon):
    cursor.execute("SELECT JernbanestasjonNavn FROM Jernbanestasjon")
    gyldige = [item[0] for item in cursor.fetchall()]
    if jernbanestasjon not in gyldige:
        return False
    else:
        return True
    
def finneStartstasjonavganger(dag, jernbanestasjon):
    cursor.execute("""SELECT Togforekomst.Dag, Togrute.TogruteID, Startstasjon.Avgangstid, Togrute.Endestasjon 
                            FROM ((Togforekomst inner join Togrute on Togforekomst.TogruteID = Togrute.TogruteID) inner join Startstasjon on Togforekomst.TogruteID = Startstasjon.TogruteID)
                            WHERE Dag = ? and JernbanestasjonNavn = ?""", (dag, jernbanestasjon))
    return cursor.fetchall()

def finneMellomstasjonavganger(dag, jernbanestasjon):
    cursor.execute("""SELECT Togforekomst.Dag, Togrute.TogruteID, Mellomstasjon.Avgangstid, Togrute.Endestasjon 
                            FROM ((Togforekomst inner join Togrute on Togforekomst.TogruteID = Togrute.TogruteID) inner join Mellomstasjon on Togforekomst.TogruteID = Mellomstasjon.TogruteID)
                            WHERE Dag = ? and JernbanestasjonNavn = ?""", (dag, jernbanestasjon))
    return cursor.fetchall()

def finneEndestasjonavganger(dag, jernbanestasjon):
    cursor.execute("""SELECT Togforekomst.Dag, Togrute.TogruteID, Endestasjon.Ankomsttid, Togrute.Endestasjon 
                            FROM (Togforekomst inner join Togrute on Togforekomst.TogruteID = Togrute.TogruteID) inner join Endestasjon on Togforekomst.TogruteID = Endestasjon.TogruteID
                            WHERE Dag = ? and JernbanestasjonNavn = ?""", (dag, jernbanestasjon))
    return cursor.fetchall()


hentetogruter()

con.commit()
con.close()